DROP TABLE IF EXISTS casfri_web.cc_polygons_simplified3;
CREATE UNLOGGED TABLE casfri_web.cc_polygons_simplified3 AS
  SELECT id,
  crown_closure_classes,
  ST_Simplify(geometry, 2000) AS geometry
  FROM casfri_web.crownclosure_polygons;

ALTER TABLE casfri_web.cc_polygons_simplified3
ADD CONSTRAINT cc_polygons_simplified3_pk PRIMARY KEY (id);

CREATE INDEX cc_polygons_simplified3_geom_idx
ON casfri_web.cc_polygons_simplified3 USING gist(geometry);

CREATE INDEX cc_polygons_simplified3_cc_idx
ON casfri_web.cc_polygons_simplified3 USING btree(crown_closure_classes);
