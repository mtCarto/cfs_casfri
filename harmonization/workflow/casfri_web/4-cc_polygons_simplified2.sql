DROP TABLE IF EXISTS casfri_web.cc_polygons_simplified2;
CREATE UNLOGGED TABLE casfri_web.cc_polygons_simplified2 AS
  SELECT id,
  crown_closure_classes,
  ST_Simplify(geometry, 1000) AS geometry
  FROM casfri_web.crownclosure_polygons;

ALTER TABLE casfri_web.cc_polygons_simplified2
ADD CONSTRAINT cc_polygons_simplified2_pk PRIMARY KEY (id);

CREATE INDEX cc_polygons_simplified2_geom_idx
ON casfri_web.cc_polygons_simplified2 USING gist(geometry);

CREATE INDEX cc_polygons_simplified2_cc_idx
ON casfri_web.cc_polygons_simplified2 USING btree(crown_closure_classes);
