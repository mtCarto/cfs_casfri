DROP TABLE IF EXISTS casfri_web.cc_polygons_simplified4;
CREATE UNLOGGED TABLE casfri_web.cc_polygons_simplified4 AS
  SELECT id,
  crown_closure_classes,
  ST_Simplify(geometry, 4000) AS geometry
  FROM casfri_web.crownclosure_polygons
  WHERE ST_Area(geometry) > 1000000;

ALTER TABLE casfri_web.cc_polygons_simplified4
ADD CONSTRAINT cc_polygons_simplified4_pk PRIMARY KEY (id);

CREATE INDEX cc_polygons_simplified4_geom_idx
ON casfri_web.cc_polygons_simplified4 USING gist(geometry);

CREATE INDEX cc_polygons_simplified4_cc_idx
ON casfri_web.cc_polygons_simplified4 USING btree(crown_closure_classes);
