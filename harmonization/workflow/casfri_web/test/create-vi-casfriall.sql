CREATE TABLE casfri_web.vi_flat_all AS
SELECT * FROM casfri_web.casfri_flat_all WHERE geometry @ ST_MakeEnvelope(-2261537, 1289848, -1819130, 1795030, 102001);

ALTER TABLE casfri_web.vi_flat_all
ADD CONSTRAINT vi_flat_all_pk PRIMARY KEY (cas_id);

CREATE INDEX vi_flat_all_geom_idx
ON casfri_web.vi_flat_all USING gist(geometry);
