DROP TABLE IF EXISTS casfri_web.crownclosure_polygons;

CREATE UNLOGGED TABLE casfri_web.crownclosure_polygons AS

  WITH all_with_cc  AS (
    SELECT 

          -- Flag lyr1 crown closure with -1 if error code so we don't aggregate it
          CASE WHEN lyr1_crown_closure_upper >= 0 THEN
             lyr1_crown_closure_upper
          ELSE -1
          END AS lyr1_crown_closure_upper,

          -- Just set lyr2 crown closure to 0 so it does not affect aggregation with lyr1 cc
          CASE WHEN lyr2_crown_closure_upper >= 0 THEN
             lyr2_crown_closure_upper
          ELSE 0
          END AS lyr2_crown_closure_upper,

          geometry
    FROM 
      casfri_web.casfri_flat_all
  ),

  all_with_cc_percent AS (
    SELECT 

      (lyr1_crown_closure_upper::float + lyr2_crown_closure_upper::float)::int AS crown_closure_percent,
      geometry
    FROM 
      all_with_cc
    WHERE lyr1_crown_closure_upper::float >= 0
  ),

  all_ccp AS (

    SELECT
      CASE WHEN crown_closure_percent >= 75 THEN '75+'
           WHEN crown_closure_percent >= 50
                AND crown_closure_percent < 75 THEN '50-75'
           WHEN crown_closure_percent >= 25
                AND crown_closure_percent < 50 THEN '25-50'
           WHEN crown_closure_percent >= 0
                AND crown_closure_percent < 25 THEN '0-25'
           ELSE 'N/A'
           END AS crown_closure_classes,
      geometry
    FROM
      all_with_cc_percent),

  all_clustered AS (
    SELECT
      crown_closure_classes,
      ST_ClusterKMeans(geometry,2000) over (PARTITION BY crown_closure_classes) AS geometry_cid,
      geometry
    FROM
      all_ccp)
  
  SELECT
    row_number() OVER () AS id,
    crown_closure_classes,
    ST_Union(geometry) as geometry
  FROM all_clustered
  GROUP BY geometry_cid, crown_closure_classes;


ALTER TABLE casfri_web.crownclosure_polygons
ADD CONSTRAINT crownclosure_polygons_pk PRIMARY KEY (id);

CREATE INDEX crownclosure_polygons_geom_idx
ON casfri_web.crownclosure_polygons USING gist(geometry);

CREATE INDEX crownclosure_polygons_cc_idx
ON casfri_web.crownclosure_polygons USING btree(crown_closure_classes);
