-------------------------------------------------------------------------
-- Build a Flat (denormalized) version of CASFRI50 including 
-- jurisdictions
-------------------------------------------------------
-- Create a flat table based on 
-- casfri50_flat.cas_flat_all_layers_same_row
--
-- See: https://github.com/edwardsmarc/CASFRI
-- at workflow/03_flatCASFRI/flatCASFRI_all_layers_same_row.sql
-------------------------------------------------------
-- DROP TABLE IF EXISTS casfri_web.casfri_flat_all;
CREATE TABLE casfri_web.casfri_flat_all AS
SELECT j.jurisdiction, c.*
FROM casfri_web.cas_latest_jurisdictions j
INNER JOIN casfri50_flat.cas_flat_all_layers_same_row c
ON c.cas_id=j.cas_id;

-------------------------------------------------------------------------
-- Add some indexes
CREATE INDEX casfri_flat_all_casid_idx
ON casfri_web.casfri_flat_all USING btree(cas_id);

CREATE INDEX casfri_flat_all_inventory_idx
ON casfri_web.casfri_flat_all USING btree(left(cas_id, 4));
    
CREATE INDEX casfri_flat_all_jurisdiction_idx
ON casfri_web.casfri_flat_all USING btree(jurisdiction);

CREATE INDEX casfri_flat_all_geom_idx
ON casfri_web.casfri_flat_all USING gist(geometry);
