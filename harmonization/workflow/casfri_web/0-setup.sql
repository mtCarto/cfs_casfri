DROP TABLE IF EXISTS casfri_web.cas_latest_jurisdictions;
CREATE TABLE casfri_web.cas_latest_jurisdictions AS
SELECT j.jurisdiction, c.* 
FROM cas_all c
INNER JOIN 
(   SELECT distinct on (jurisdiction) jurisdiction, inventory_id
    FROM (
        SELECT LEFT(inventory_id,2) AS jurisdiction, 
               inventory_id, 
               stand_photo_year
        FROM casfri50.cas_all
    ) a
    ORDER BY jurisdiction, stand_photo_year DESC NULLS FIRST
) j ON c.inventory_id=j.inventory_id;

COMMENT ON TABLE casfri_web.cas_latest_jurisdictions IS
'The cas_all table consolidated with only the latest inventory for each jurisdiction.';

-- Add some indexes
CREATE INDEX cas_latest_jurisdictions_casid_idx
ON casfri_web.cas_latest_jurisdictions USING btree(cas_id);

CREATE INDEX cas_latest_jurisdictions_inventory_idx
ON casfri_web.cas_latest_jurisdictions USING btree(left(cas_id, 4));
    
CREATE INDEX cas_latest_jurisdictions_province_idx
ON casfri_web.cas_latest_jurisdictions USING btree(left(cas_id, 2));
