# CANFRI - harmonization

This directory includes NFIS-specific conversions & translations that are
not provided with the https://github.com/edwardsmarc/CASFRI project.

It is referenced by the https://sleet.nfis.org/NFIS/ansible/roles/ce.casfri
 ansible role when creating the CASFRI translation environment.

Note that this may eventually be merged with a fork of the github project
when the CASFRI/Laval contract is complete.

# Directory format

This repository follows the same format as the edwardsmarc/CASFRI project:

translations/tables/*

 - translation files go in this directory
 - if a translation file matches the name of a file that was first loaded
   from edwardsmarc/CASFRI, its contents will be APPENDED to the table
   by the ce.casfri ansible role

conversion/sh/*

 - conversion scripts
 - if a conversion script matches the name of a file that exists in this
   location in the edwardsmarc/CASFRI project, it will be REPLACED by this
   one

workflow/*

 - any files in this directory will be added to the workflow folder
