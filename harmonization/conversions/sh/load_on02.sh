#!/bin/bash -x

# This script loads the Ontario FIM forest inventory (ON02) into PostgreSQL

# This dataset is split into 41 tables in a single geodatabase, each representing
# a different forest management unit.

# The year of photography is included in the attributes table (YRSOURCE)

# Load into a target table in the schema defined in the config file.

# If the table already exists, it can be overwritten by setting the "overwriteFRI" variable 
# in the configuration file.

# All tables have the same core attributes, but many have additional attributes.
# Load all tables using -addfields. This will add all columns from all tables.
# Then use -sql query to select only the columns specified in the FIM documentation. 
# Drop any rows with null polyid using the sql statement.

######################################## Set variables #######################################

source ./common.sh

inventoryID=ON02
#srcFileName=ON_FRI_forest
srcFileName=INV2019
srcFullPath="$friDir/ON/$inventoryID/data/inventory/$srcFileName.gdb"
fullTargetTableName=$targetFRISchema.on02
temp_table=$targetFRISchema.on02_all_attributes

ogr_options="$overwrite_tab"

if [ $gdal_1_11_4 == True ]; then
  gdal_3_options=
else 
  gdal_3_options="-nlt CONVERT_TO_LINEAR --config OGR_SKIP FileGDB"
fi


########################################## Process ######################################
# drop any temp tables from previous loads
"$gdalFolder/ogrinfo" "$pg_connection_string" \
-sql "
DROP TABLE IF EXISTS $temp_table;
"

for F in FC451 Park_Wabakimi Park_Quetico eFRI_CatLake FC815 FC680 FC060 FC120 FC175 FC220 FC535 FC615 FC702 FC889 Park_LkSuperior Park_Pukaskwa Park_WCaribou FC438 FC796 FC930 FC177 FC415 FC406 FC840 FC035 FC130 FC230 FC966 FC601 FC360 FC754 FC898 FC574 FC565 FC509 FC280 FC210 FC994 FC110 FC390 FC421 FC350 FC780 FC140 FC644 FC490 FC443
do
    "$gdalFolder/ogr2ogr" \
    -f PostgreSQL "$pg_connection_string" "$srcFullPath" \
    -nln $temp_table\
    -nlt PROMOTE_TO_MULTI $gdal_3_options \
    -progress \
    -sql "SELECT *, '$F' AS src_filename, '$inventoryID' AS inventory_id  FROM $F" \
    $layer_creation_options $other_options \
    $overwrite_option

    overwrite_option="-update -append -addfields"
    layer_creation_options=""
done

# Use sql to select inventory columns for final on02 table
"$gdalFolder/ogrinfo" "$pg_connection_string" \
-sql "
DROP TABLE IF EXISTS $fullTargetTableName; 
CREATE TABLE $fullTargetTableName AS
SELECT wkb_geometry, ogc_fid, inventory_id, src_filename, SHAPE_AREA AS area, SHAPE_LENGTH AS perimeter, polyid, substring(polyid, 1, 10) AS polyid_1_10, substring(polyid, 11, 10) AS polyid_11_20, polytype, yrsource, source, formod, devstage, yrdep, deptype,
       oyrorg, ospcomp, oleadspc, oage, oht, occlo, osi, osc, uyrorg, uspcomp, uleadspc, uage, uht, ucclo, usi, usc,
       incidspc, vert, horiz, pri_eco, sec_eco, access1, access2, mgmtcon1, mgmtcon2, mgmtcon3
FROM $temp_table
WHERE polyid IS NOT NULL;

DROP TABLE IF EXISTS $temp_table;
"

createSQLSpatialIndex=True

source ./common_postprocessing.sh
