# CANFRI

This repository houses CFS-specific information and scripts for the input, harmonization, and output (dissemination) of CASFRI/CANFRI data.

The root directory of this project has the following subdirectories:

# Directory format

input/
 - this contains scripts and information related to raw input datasets and the act of receiving data for inclusion into CASFRI

harmonization/
 - this contains CFS-specific scripts for conversion and translation of raw data into CASFRI

output/
 - this contains scripts and information used in the dissemination of CASFRI

See also readme.md files in each subdirectory for further information.

# Dataset notes

See input/readme.md
