# CANFRI - INPUT

This directory contains information and scripts related to input datasets.

# Dataset notes

## QC

There are 2 types of datasets available on the QC.  The explanation of the difference is as follows:

  The original forest map (ORI) is the result of the interpretation of aerial
  photographs and control points in the field as part of the ecoforest 
  inventory of southern Quebec. This mapping presents the various forest and 
  ecological characteristics of the forest area and corresponds to the portrait
  of the forest up to the year the aerial photograph was taken (mapping cycle
  of approximately 10 years).
  Then, the contours and the nature of recent disturbances (forest 
  interventions, fires and other disturbances) are integrated annually into it
  to produce the forest map with disturbances (MAJ).

The _10 and _93 suffixes refer to the ArcGIS version used to produce the geodatabase (ArcGIS v10 or v9.3).

ORI: 
  url: ftp://transfert.mffp.gouv.qc.ca/Public/Diffusion/DonneeGratuite/Foret/DONNEES_FOR_ECO_SUD/Resultats_inventaire_et_carte_ecofor/
  files:
    - CARTE_ECO_ORI_PROV_10.zip
    - CARTE_ECO_ORI_PROV_93.zip

MAJ: 
  url: ftp://transfert.mffp.gouv.qc.ca/Public/Diffusion/DonneeGratuite/Foret/DONNEES_FOR_ECO_SUD/Cartes_ecoforestieres_perturbations
  files:
    - CARTE_ECO_MAJ_PROV_10.zip
    - CARTE_ECO_MAJ_PROV_93.zip


The geodatabases in these files are complex.  From Melina Houle (who was tasked with translating them):
  It is quite complex. Quebec mix many inventories in the same gdb where the 
  meaning of a code for a specific attributes changed through time and it is 
  not clear when exactly that happen. They have the 3rd inventory, the 4th 
  INITIALE (similar to the 3rd, but meaning of the code changed), the 4th MIXTE
  (which is a mix of 4 INITIALE and 5), the 4 NAIPF (same as the 5th) and the 
  5th.   
  The 5th inventory is similar to others provinces, but the 3rd and the 4th
  come from another world. In the past, they didn't identify species and 
  percent per polygon. They used an attributes called species group that 
  mixed species and a range of percent. And they have 65,000 different 
  combinations with a funny way to map age which makes the LYR table hard to
  translate.
 
  There is not a complete coverage by the 4th and the 5th. They finished the 4th
  the same year they were starting the 5th.  Right now, the 5th covers 1/4, the
  4th is 2/3 and the 3rd the rest. Quebec 5th will be done by 2028 
  (https://mffp.gouv.qc.ca/forets/inventaire/pdf/resultats-compilation-5e.pdf). 
  Previous version was a mixed of 3rd and a ongoing 4th at the time.

  Decided to split the single source table into three inventories that we have
  named QC03, QC04 and QC05. These will each be treated as separate inventories
  and will have their own sets of translation tables. The tables used for QC05
  will be the 5th inventory rules that will be used for future datasets released
  in QC. Initially it seems that splitting the data this way will make the 
  translations much simpler, albeit with three times as many tables to 
  translate, validate and test.

  I should also mention this approach is similar to MB where the MB05 and MB06
  data are contained in a single source table, even though they use completely
  different standards. As with QC, we split the data during loading.
