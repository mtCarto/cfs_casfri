# CANFRI - OUTPUT

This directory includes information and scripts for the dissemination of CASFRI.
This will eventually be referenced by Ansible scripts when provisioning CASFRI on an NFIS node.

# Directory format

rendering/SLDs

 - SLD files for rendering CASFRI on Geoserver
